
Mi primer proyecto con NodeJS y express (Folder red-bicicletas):

El proyecto debe cumplir con los siguientes puntos:

1.- un archivo README (sólo texto) en el repositorio de Bitbucket. (este mismo archivo).
2.- el mensaje de bienvenida a Express.
3.- el proyecto vinculado con el repositorio de Bitbucket creado previamente. https://edgaalejandrotorres@bitbucket.org/edgaalejandrotorres/proyecto1.git
4.- el servidor que se ve correctamente, comparándolo con la versión original.
5- un mapa centrado en una ciudad.
6- marcadores indicando una  ubicación.
7.- un script npm en el archivo package.json, debajo del “start”, que se llama “devstart” y que levanta el servidor utilizando npdemon.
8.- un par de bicicletas en la colección que conoce la bicicleta en memoria, con las ubicaciones cercanas al centro del mapa agregado.
9.- una colección del modelo de bicicleta.
10.- los endpoints de la API funcionando con Postman.

https://edgaalejandrotorres@bitbucket.org/edgaalejandrotorres/proyecto1.git
