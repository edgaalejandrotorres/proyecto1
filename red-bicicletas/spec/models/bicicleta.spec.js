var Bicicleta = require("../../models/bicicleta");

beforeEach(() => { Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () =>{
	it('comienza vacia', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});

describe('Bicicleta.add', () => {
	it('agregamos una', () =>{
		expect(Bicicleta.allBicis.length).toBe(0);
		
		var a = new Bicicleta(1, 'Azul', 'GT', [31.855752, -116.6199275]);
		Bicicleta.add(a);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	});
});

describe('Bicicleta.findById', () => {
	it('debe devolver la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici = new Bicicleta(1, 'Roja', 'Canondale', [31.855752, -116.6199275]);
		var aBici2 = new Bicicleta(2, 'Verde', 'Huffy', [31.855752, -116.6199275]);
		Bicicleta.add(aBici);
		Bicicleta.add(aBici2);
		
		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe("Roja");
		expect(targetBici.modelo).toBe("Canondale");
	});
});

describe('Bicicleta.removeById', () =>{
	it('debe remover la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
		var aBici = new Bicicleta(1, 'Roja', 'Canondale', [31.855752, -116.6199275]);
		Bicicleta.add(aBici);
		expect(Bicicleta.allBicis.length).toBe(1);
		
		Bicicleta.removeById(1);		
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});