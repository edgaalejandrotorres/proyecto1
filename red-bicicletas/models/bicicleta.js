const { bicicleta_create_get } = require("../controllers/bicicleta");

// Model
var Bicicleta = function(id, color, modelo, ubicacion){
    this.id = id; 
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

    Bicicleta.prototype.toString = function(){
        return 'id: '+ this.id + ' color:' + this.color + ' modelo:' + this.modelo + ' ubicacion:' + this.ubicacion;
    }

    Bicicleta.allBicis = [];
    Bicicleta.add = function(aBici){
        Bicicleta.allBicis.push(aBici);
    }

    Bicicleta.findById = function(aBiciId){
        var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)
        if (aBici)
            return aBici;
        else
            throw new Error(`No existe una bicicleta con el Id ${aBiciId}`);
    }

    Bicicleta.removeById = function(aBiciId){
        //var aBici = Bicicleta.findById(aBiciId);
        for(var i=0;i<Bicicleta.allBicis.length;i++){
            if (Bicicleta.allBicis[i].id == aBiciId){
                Bicicleta.allBicis.splice(i, 1);
                break;
            }
        }
    }

/*
var a = new Bicicleta(1, 'Azul', 'GT', [31.855752, -116.6199275]);
var b = new Bicicleta(2, 'Blanca', 'Mongoose', [31.8565084, -116.6199382]);
var c = new Bicicleta(3, 'Negra', 'Trek', [31.8575017, -116.6213866]);
var d = new Bicicleta(4, 'Gris', 'Cannondale', [31.8555515, -116.6204532]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);
*/

module.exports = Bicicleta;
